package com.beingjavaguys.scheduler;

import com.beingjavaguys.controller.DataController;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Alterovych Ilya
 */
public class SchedulerHelper {

    @Autowired
    DataController dataController;

    public void scheduler() {
        dataController.init();
    }
}
