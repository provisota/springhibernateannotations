package com.beingjavaguys.dao;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.beingjavaguys.domain.Employee;

public class DataDaoImpl implements DataDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public int insertRow(Employee employee) {
//        encodeRequest(employee);
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(employee);
        tx.commit();
        Serializable id = session.getIdentifier(employee);
        session.close();
        return (Integer) id;
    }

    @Override
    public List<Employee> getList() {
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<Employee> employeeList = session.createQuery("from Employee")
                .list();
        session.close();
        return employeeList;
    }

    @Override
    public Employee getRowById(int id) {
        Session session = sessionFactory.openSession();
        Employee employee = (Employee) session.load(Employee.class, id);
        return employee;
    }

    @Override
    public int updateRow(Employee employee) {
//        encodeRequest(employee);
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(employee);
        tx.commit();
        Serializable id = session.getIdentifier(employee);
        session.close();
        return (Integer) id;
    }

    @Override
    public int deleteRow(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Employee employee = (Employee) session.load(Employee.class, id);
        session.delete(employee);
        tx.commit();
        Serializable ids = session.getIdentifier(employee);
        session.close();
        return (Integer) ids;
    }

    private static void encodeRequest(Employee employee) {
        try {
            employee.setFirstName(
                    new String(employee.getFirstName().getBytes("ISO-8859-1"), "utf8"));
            employee.setLastName(
                    new String(employee.getLastName().getBytes("ISO-8859-1"), "utf8"));
            employee.setEmail(
                    new String(employee.getEmail().getBytes("ISO-8859-1"), "utf8"));
            employee.setPhone(
                    new String(employee.getPhone().getBytes("ISO-8859-1"), "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
