-- Dumping database structure for springhibernate_db
CREATE DATABASE IF NOT EXISTS `springhibernate_db` DEFAULT CHARACTER SET utf8;
USE `springhibernate_db`;


-- Dumping structure for table springhibernate_db.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Dumping data for table springhibernate_db.employee: ~3 rows (approximately)
ALTER TABLE `employee` DISABLE KEYS;
INSERT INTO `employee` (`first_name`, `last_name`, `email`, `phone`) VALUES
 ('Virat', 'Kohli', 'virat@beingjavaguys.com', '89876787890'),
 ('Sachin', 'Tendulkar', 'sachin@india.com', '89898989898'),
 ('Virendra', 'Sehwag', 'viru@delhi.com', '8976778789');
